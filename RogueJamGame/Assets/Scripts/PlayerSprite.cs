using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprite: MonoBehaviour {
  [SerializeField]
  SpriteRenderer _spriteRenderer;

  [SerializeField]
  Sprite _faceDown;

  [SerializeField]
  Sprite _faceUp;

  [SerializeField]
  Sprite _faceLeft;

  [SerializeField]
  Sprite _faceRight;

  public void SetDirection(float degrees) {
    if ((degrees >= -45 && degrees < 0) || (degrees >= 0 && degrees < 45)) {
      _spriteRenderer.sprite = _faceRight;
    }
    else if (degrees >= 45 && degrees < 135) {
      _spriteRenderer.sprite = _faceUp;
    }
    else if (degrees >= 135 || degrees < -135) {
      _spriteRenderer.sprite = _faceLeft;
    }
    else if (degrees >= -135 && degrees < -45) {
      _spriteRenderer.sprite = _faceDown;
    }
  }
}
