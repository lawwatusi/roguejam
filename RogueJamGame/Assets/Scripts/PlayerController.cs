using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector2 = UnityEngine.Vector2;

public class PlayerController: MonoBehaviour {
  [SerializeField] float _moveSpeed = 6;
  [SerializeField] float _postAttackCooldown = 0.3f;

  [SerializeField] Rigidbody2D _rigidbody2D;
  [SerializeField] PlayerSprite _sprite;
  [SerializeField] GameObject _interactor;
  [SerializeField] AttackCollider _attackCollider;
  [SerializeField] InteractCollider _interactCollider;

  float _postAttackCooldownTimer = 0f;

  void Update() {
    if (_attackCollider.IsMidAttack) {
      // if we're attacking, we're too busy to do other stuff lolol
      return;
    }

    if (_postAttackCooldownTimer > 0f) {
      _postAttackCooldownTimer -= Time.deltaTime;
    }

    var moveInput = GetMoveInput();
    _rigidbody2D.velocity = moveInput * _moveSpeed;
    if (moveInput.magnitude > 0) {
      float angle = Utils.SignedAngleSnap90FromVector(moveInput);
      _sprite.SetDirection(angle);
      var interactorRotation = _interactor.transform.rotation;
      _interactor.transform.rotation = Quaternion.Euler(interactorRotation.x, interactorRotation.y, angle);
    }

    var firePressed = Input.GetButtonDown("Fire1");
    var interactPressed = Input.GetButtonDown("Jump");
    if (firePressed && !_attackCollider.IsMidAttack && _postAttackCooldownTimer <= 0f) {
      _rigidbody2D.velocity = Vector2.zero;
      _postAttackCooldownTimer = _postAttackCooldown;
      _attackCollider.StartAttack();
    }
    else if (interactPressed) {
      var interactTargets = _interactCollider.InteractTargets;
      for (int i = 0; i < interactTargets.Count; ++i) {
        interactTargets[i].OnInteract.Invoke();
      }
    }
  }

  Vector2 GetMoveInput() {
    var rawMoveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
    if (rawMoveInput.magnitude < 0.1) {
      return Vector2.zero;
    }
    return Utils.Clamp(rawMoveInput, 1);
  }
}
