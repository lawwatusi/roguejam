using System.Collections;
using System.Collections.Generic;
using UnityEngine;

  public class AttackCollider: MonoBehaviour {
    [SerializeField] BoxCollider2D _collider2D;
    [SerializeField] SpriteRenderer _spriteRenderer;

    [SerializeField] float _attackDuration = 10f / 60f;

    float _attackTimer = 0f;
    Vector2 _initialColliderSize;
    Color _currentSpriteColor = Color.white;

    public bool IsMidAttack => _attackTimer > 0f;

    void Awake() {
      _initialColliderSize = _collider2D.size;
      FinishAttack();
    }

    void Update() {
      if (_attackTimer <= 0f) {
        return;
      }

      _attackTimer -= Time.deltaTime;
      float attackPhase = 1f - (_attackTimer / _attackDuration);
      _currentSpriteColor.a = 1f - attackPhase * attackPhase;
      _spriteRenderer.color = _currentSpriteColor;
      if (_attackTimer <= 0f) {
        FinishAttack();
      }
    }

    public void StartAttack() {
      _collider2D.size = _initialColliderSize;
      _currentSpriteColor.a = 1f;
      _spriteRenderer.color = _currentSpriteColor;
      _attackTimer = _attackDuration;
    }

    void FinishAttack() {
      _collider2D.size = Vector2.zero;
      _currentSpriteColor.a = 0f;
      _spriteRenderer.color = _currentSpriteColor;
      _attackTimer = 0f;
    }
  }
