﻿using System;
using UnityEngine;

public class Utils {
  /**
   * Ensure the vector `v` has a magnitude less than or equal to `length`. This is different from
   * `v.normalized` because Clamp can return a vector with a magnitude less than 1.
   */
  public static Vector2 Clamp(Vector2 v, float length) {
    if (v.magnitude <= length) {
      return v;
    }
    return v.normalized * length;
  }

  public static float SignedAngle(Vector2 v) {
    return Vector2.SignedAngle(Vector2.right, v);
  }

  /**
   * Calculates the signed angle of the given vector.
   *
   * Returns a 45-degree angle, like 0, 45, 90, or -135.
   */
  public static float SignedAngleSnap45FromVector(Vector2 v) {
    float degrees = SignedAngle(v);
    return SnapSignedAngleTo45(degrees);
  }

  public static float SnapSignedAngleTo45(float degrees) {
    if ((degrees >= -22.5 && degrees < 0) || (degrees >= 0 && degrees < 22.5)) {
      return 0;
    }
    if (degrees >= 22.5 && degrees < 67.5) {
      return 45;
    }
    if (degrees >= 67.5 && degrees < 112.5) {
      return 90;
    }
    if (degrees >= 112.5 && degrees < 157.5) {
      return 135;
    }
    if ((degrees >= 157.5 && degrees <= 180) || (degrees >= -180 && degrees < -157.5)) {
      return -180;
    }
    if (degrees >= -157.5 && degrees < -112.5) {
      return -135;
    }
    if (degrees >= -112.5 && degrees < -67.5) {
      return -90;
    }
    if (degrees >= -67.5 && degrees < -22.5) {
      return -45;
    }
    Debug.LogWarning("unexpected code branch reached (SnapSignedAngleTo45)");
    return 0;
  }

  /**
   * Calculates the signed angle of the given vector.
   *
   * Returns 0, 90, 180, or -90.
   *
   * 0 is right, 90 is up (positive Y), 180 is left, -90 is down.
   *
   * If the angle of the vector is exactly 45 or 135 degrees, 90/up is returned.
   * If the angle of the vector is exactly -45 or -135 degrees, -90/down is returned.
   */
  public static float SignedAngleSnap90FromVector(Vector2 v) {
    float degrees = SignedAngle(v);
    return SnapSignedAngleTo90(degrees);
  }

  public static float SnapSignedAngleTo90(float degrees) {
    if ((degrees > -45 && degrees < 0) || (degrees >= 0 && degrees < 45)) {
      return 0;
    }
    else if (degrees >= 45 && degrees <= 135) {
      return 90;
    }
    else if (degrees > 135 || degrees < -135) {
      return -180;
    }
    else if (degrees >= -135 && degrees <= -45) {
      return -90;
    }
    Debug.LogWarning("unexpected code branch reached (SnapSignedAngleTo90)");
    return 0;
  }
}
