using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;

public class Interactable: MonoBehaviour {
  [SerializeField] UnityEvent _onInteract;
  public UnityEvent OnInteract => _onInteract;

  [SerializeField]
  UnityEvent _onHoverStart;
  [SerializeField]
  UnityEvent _onHoverEnd;

  public void OnHoverStart() {
    InteractBubbleController[] bubbles = GetComponentsInChildren<InteractBubbleController>();
    for (int i = 0; i < bubbles.Length; ++i) {
      bubbles[i].Show();
    }
    _onHoverStart.Invoke();
  }

  public void OnHoverEnd() {
    InteractBubbleController[] bubbles = GetComponentsInChildren<InteractBubbleController>();
    for (int i = 0; i < bubbles.Length; ++i) {
      bubbles[i].Hide();
    }
    _onHoverEnd.Invoke();
  }
}
