using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignController: MonoBehaviour {
  [SerializeField] RPGTalk _rpgTalk;
  [SerializeField, TextArea] string _message;

  // the RPGTalk devs made a mistake !
  TextAsset _textAssetForMessage;

  void Awake() {
    _textAssetForMessage = new TextAsset(_message);
    _rpgTalk.txtToParse = _textAssetForMessage;
  }

  public void ReadSign() {
    if (!_rpgTalk.isPlaying) {
      _rpgTalk.NewTalk();
    }
  }

  public void LeaveHover() {
    if (_rpgTalk.isPlaying) {
      _rpgTalk.EndTalk();
    }
  }
}
